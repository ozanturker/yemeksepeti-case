from django.forms import model_to_dict
from rest_framework import status, mixins
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet

from yemeksepeti.orders.models import Order
from yemeksepeti.orders.serializers import (OrderSerializer,
                                            ProcessOrderSerializer)
from yemeksepeti.orders.services import OrderService


class OrderViewSet(mixins.CreateModelMixin,
                   mixins.RetrieveModelMixin,
                   mixins.ListModelMixin,
                   GenericViewSet):
    service = OrderService()
    serializers = {
        'default': OrderSerializer,
        'process_order': ProcessOrderSerializer
    }
    queryset = Order.objects.all()

    def get_serializer_class(self):
        return self.serializers.get(self.action,
                                    self.serializers['default'])

    def get_queryset(self):
        queryset = Order.objects.all()
        status = self.request.query_params.get('status')
        if status is not None:
            queryset = queryset.filter(status=status)
        return queryset

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        result = self.service.create_order(**serializer.validated_data)
        return Response(model_to_dict(result), status=status.HTTP_201_CREATED)

    @action(detail=False, methods=['POST'], name='process_order',
            url_path='process')
    def process_order(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        response = self.service.process_order(**serializer.validated_data)
        serializer = OrderSerializer(data=response, many=True)
        serializer.is_valid()
        return Response(serializer.data, status=status.HTTP_200_OK)
