from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from yemeksepeti.commons.factories.factory import UserFactory


class UsersViewTest(TestCase):

    def test_create_user_success(self):
        data = {
            'email': 'test@email.com',
            'first_name': 'Name',
            'last_name': 'Surname'
        }
        response = self.client.post(
            reverse('categories:users-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = response.json()

        self.assertEqual(response.get('email'), data.get('email'))
        self.assertEqual(response.get('first_name'), data.get('first_name'))
        self.assertEqual(response.get('last_name'), data.get('last_name'))

    def test_create_user_invalid_data(self):
        data = {
            'email': 'test@email.com',
            'last_name': 'Surname'
        }
        response = self.client.post(
            reverse('categories:users-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        required_error = response.json().get('first_name')

        self.assertEqual(required_error, ['This field is required.'])

    def test_create_user_duplicated_data(self):
        user = UserFactory.create()
        data = {
            'email': user.email,
            'first_name': 'Name',
            'last_name': 'Surname'
        }
        response = self.client.post(
            reverse('categories:users-list'), data=data)

        email_error = response.json().get('email')

        self.assertEqual(email_error, [
            'user with this email already exists.'])

    def test_get_all_users_success(self):
        user = UserFactory.create()

        response = self.client.get(
            reverse('categories:users-list', ))

        user_mail = response.json()[0].get('email')

        self.assertEqual(user_mail, user.email)

    def test_get_user_by_id(self):
        user = UserFactory.create()
        response = self.client.get(
            reverse('categories:users-detail',
                    args=(user.pk,)))

        user_mail = response.json().get('email')

        self.assertEqual(user_mail, user.email)

    def test_get_user_by_invalid_id(self):
        response = self.client.get(
            reverse('categories:users-detail',
                    args=(1,)))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json().get('detail'), 'Not found.')

    def test_update_user(self):
        user = UserFactory.create()

        data = {
            'email': 'test@email.com',
            'first_name': 'Name',
            'last_name': 'Surname'
        }

        url = reverse('categories:users-detail',
                      args=(user.pk,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        email = response.json().get('email')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(email, data.get('email'))

    def test_update_user_invalid_id(self):
        data = {
            'email': 'test@email.com',
            'first_name': 'Name',
            'last_name': 'Surname'
        }
        url = reverse('categories:users-detail', args=(1,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_user(self):
        user = UserFactory.create()

        url = reverse('categories:users-detail',
                      args=(user.pk,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_category_invalid_id(self):
        url = reverse('categories:users-detail',
                      args=(1,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
