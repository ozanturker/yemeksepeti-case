from django.forms import model_to_dict
from rest_framework import viewsets, status
from rest_framework.response import Response

from yemeksepeti.restaurants.models import Restaurant
from yemeksepeti.restaurants.serializers import RestaurantSerializer
from yemeksepeti.restaurants.services import RestaurantService


class RestaurantViewSet(viewsets.ModelViewSet):
    service = RestaurantService()
    queryset = Restaurant.objects.all()
    serializer_class = RestaurantSerializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        result = self.service.create_restaurant(**serializer.validated_data)
        return Response(model_to_dict(result), status=status.HTTP_201_CREATED)

    def destroy(self, request, *args, **kwargs):
        try:
            self.service.delete_restaurant(restaurant_id=kwargs['pk'])
        except Restaurant.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        return Response(status=status.HTTP_204_NO_CONTENT)
