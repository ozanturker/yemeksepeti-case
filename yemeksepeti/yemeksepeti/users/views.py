from rest_framework import viewsets

from yemeksepeti.users.models import User
from yemeksepeti.users.serializers import UserSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
