import json

from django.db.transaction import atomic

from yemeksepeti.commons.clients.pubsub_client import SNSPubSubClient
from yemeksepeti.commons.clients.queue_client import SQSClient
from yemeksepeti.orders.models import Order, OrderItem, OrderStatus
from yemeksepeti.restaurants.models import Restaurant


class OrderService:
    sns_client = SNSPubSubClient()
    sqs_client = SQSClient()

    @atomic
    def create_order(self, customer, restaurant, status, order_items):
        order = Order.objects.create(customer=customer, status=status,
                                     restaurant=restaurant)
        self._create_order_items(order, order_items)
        self.sns_client.publish_message(
            restaurant.order_topic_address,
            content=json.dumps({
                'order_id': order.pk,
                'customer': order.customer.pk,
                'status': order.status
            }))
        return order

    @atomic
    def process_order(self, restaurant_id, status):
        processed_msgs = []
        processed_orders = []
        restaurant = Restaurant.objects.get(pk=restaurant_id)
        new_order_msgs = self.sqs_client.get_messages(restaurant.code)
        for order_msg in new_order_msgs.get('Messages', []):
            order_msg_str = order_msg.get('Body')
            order_msg_json = json.loads(order_msg_str)
            json_msg = order_msg_json.get('Message')
            json_msg = json.loads(json_msg)
            order = Order.objects.get(pk=json_msg.get('order_id'))
            if order.status == OrderStatus.WAITING:
                order.status = status
                processed_msgs.append({
                    'ReceiptHandle': order_msg.get('ReceiptHandle'),
                    'Id': order_msg.get('MessageId')})
                processed_orders.append(order)

        Order.objects.bulk_update(processed_orders, ['status'])
        self.sqs_client.delete_messages(restaurant.code, processed_msgs)
        return processed_orders

    def _create_order_items(self, order, order_items: list):
        order_items_obj = []
        for order_item in order_items:
            order_items_obj.append(
                OrderItem(order=order, food=order_item.get('food'),
                          quantity=order_item.get('quantity')))

        OrderItem.objects.bulk_create(order_items_obj)
