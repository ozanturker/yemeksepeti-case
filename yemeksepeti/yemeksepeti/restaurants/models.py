from django.db import models


class Restaurant(models.Model):
    code = models.CharField(max_length=255, unique=True)
    name = models.CharField(max_length=255, unique=True)
    address = models.TextField()
    order_topic_address = models.CharField(max_length=255, unique=True)
    order_queue_address = models.CharField(max_length=255, unique=True)
