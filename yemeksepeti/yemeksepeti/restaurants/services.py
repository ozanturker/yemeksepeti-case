import uuid

from django.db.transaction import atomic

from yemeksepeti.commons.clients.pubsub_client import SNSPubSubClient
from yemeksepeti.commons.clients.queue_client import SQSClient
from yemeksepeti.restaurants.models import Restaurant


class RestaurantService:
    sns_client = SNSPubSubClient()
    sqs_client = SQSClient()

    @atomic
    def create_restaurant(self, name: str, address: str):
        restaurant_uuid = str(uuid.uuid4().hex)
        queue = self._create_restaurant_order_queue(restaurant_uuid)
        queue_arn = self.sqs_client.get_queue_arn(queue.get('QueueUrl'))
        restaurant_topic = self._create_restaurant_order_topic(restaurant_uuid)
        self.sns_client.subscribe_queue(restaurant_topic.get('TopicArn'),
                                        queue_arn)
        self.sqs_client.create_permission(queue.get('QueueUrl'), queue_arn,
                                          restaurant_topic.get('TopicArn'))
        restaurant = self._save_to_db(restaurant_uuid, address, name,
                                      restaurant_topic.get('TopicArn'),
                                      queue_arn)
        return restaurant

    def _save_to_db(self, code, address, name, topic_arn, queue_arn):
        restaurant = Restaurant(code=code, name=name, address=address,
                                order_topic_address=topic_arn,
                                order_queue_address=queue_arn)
        restaurant.save()
        return restaurant

    def _create_restaurant_order_topic(self, topic_id):
        return self.sns_client.create_topic(topic_id)

    def _create_restaurant_order_queue(self, queue_id):
        return self.sqs_client.create_queue(queue_id)

    @atomic
    def delete_restaurant(self, restaurant_id):
        restaurant = Restaurant.objects.get(pk=restaurant_id)
        self.sqs_client.delete_queue(restaurant.code)
        self.sns_client.delete_topic(restaurant.order_topic_address)
        restaurant.delete()
