from unittest.mock import patch

from django.db import IntegrityError
from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from yemeksepeti.commons.factories.factory import RestaurantFactory
from yemeksepeti.restaurants.models import Restaurant
from yemeksepeti.restaurants.services import RestaurantService


class RestaurantServiceTest(TestCase):

    @patch('yemeksepeti.commons.clients.queue_client.SQSClient'
           '.create_permission')
    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.subscribe_queue')
    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.create_topic')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.get_queue_arn')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.create_queue')
    def test_create_restaurant(self, mock_queue, mock_queue_arn,
                               mock_create_topic, mock_subs_queue,
                               mock_create_permission):
        mock_queue.return_value = {
            'QueueUrl': "www.queueurl.com"
        }
        mock_queue_arn.return_value = "arn://mock_queue_arn"
        mock_create_topic.return_value = {
            'TopicArn': "arn://mock_topic_arn"
        }
        mock_subs_queue.return_value = None

        mock_create_permission.return_value = None

        service = RestaurantService()
        service.create_restaurant('testname', 'test address')

        restaurant = Restaurant.objects.get(name='testname')

        self.assertEqual(restaurant.order_topic_address,
                         'arn://mock_topic_arn')
        self.assertEqual(restaurant.name, 'testname')
        self.assertEqual(restaurant.address, 'test address')
        self.assertEqual(restaurant.order_queue_address,
                         'arn://mock_queue_arn')

    @patch('yemeksepeti.commons.clients.queue_client.SQSClient'
           '.create_permission')
    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.subscribe_queue')
    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.create_topic')
    @patch(
        'yemeksepeti.commons.clients.queue_client.SQSClient.get_queue_arn')
    @patch(
        'yemeksepeti.commons.clients.queue_client.SQSClient.create_queue')
    def test_create_restaurant_duplicated_name(self, mock_queue,
                                               mock_queue_arn,
                                               mock_create_topic,
                                               mock_subs_queue,
                                               mock_create_permission):
        restaurant = RestaurantFactory.create()

        mock_queue.return_value = {
            'QueueUrl': "www.queueurl.com"
        }
        mock_queue_arn.return_value = "arn://mock_queue_arn"
        mock_create_topic.return_value = {
            'TopicArn': "arn://mock_topic_arn"
        }
        mock_subs_queue.return_value = None

        mock_create_permission.return_value = None

        service = RestaurantService()
        with self.assertRaises(IntegrityError):
            service.create_restaurant(restaurant.name, 'test address')

    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.delete_topic')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.delete_queue')
    def test_delete_restaurant(self, mock_delete_queue, mock_delete_topic):
        restaurant = RestaurantFactory.create()

        service = RestaurantService()
        service.delete_restaurant(restaurant_id=restaurant.pk)

        with self.assertRaises(Restaurant.DoesNotExist):
            Restaurant.objects.get(pk=restaurant.pk)

    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.delete_topic')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.delete_queue')
    def test_delete_restaurant_not_exist(self, mock_delete_queue,
                                         mock_delete_topic):

        service = RestaurantService()
        with self.assertRaises(Restaurant.DoesNotExist):
            service.delete_restaurant(restaurant_id=1)


class RestaurantViewTest(TestCase):

    @patch('yemeksepeti.commons.clients.queue_client.SQSClient'
           '.create_permission')
    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.subscribe_queue')
    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.create_topic')
    @patch(
        'yemeksepeti.commons.clients.queue_client.SQSClient.get_queue_arn')
    @patch(
        'yemeksepeti.commons.clients.queue_client.SQSClient.create_queue')
    def test_create_restaurant_success(self, mock_queue, mock_queue_arn,
                                       mock_create_topic, mock_subs_queue,
                                       mock_create_permission):
        mock_queue.return_value = {
            'QueueUrl': "www.queueurl.com"
        }
        mock_queue_arn.return_value = "arn://mock_queue_arn"
        mock_create_topic.return_value = {
            'TopicArn': "arn://mock_topic_arn"
        }
        mock_subs_queue.return_value = None

        mock_create_permission.return_value = None

        data = {
            'name': 'test_restaurant',
            'address': 'test_address'
        }
        response = self.client.post(
            reverse('categories:restaurants-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = response.json()

        restaurant_name = response.get('name')
        restaurant_address = response.get('address')

        self.assertEqual(restaurant_name, data.get('name'))
        self.assertEqual(restaurant_address, data.get('address'))

    def test_create_restaurant_invalid_data(self):
        data = {
            'name': 'test_restaurant'
        }
        response = self.client.post(
            reverse('categories:restaurants-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        required_error = response.json().get('address')

        self.assertEqual(required_error, ['This field is required.'])

    def test_create_restaurant_duplicated_data(self):
        restaurant = RestaurantFactory.create()
        data = {
            'name': restaurant.name,
            'address': restaurant.address
        }
        response = self.client.post(
            reverse('categories:restaurants-list'), data=data)

        restaurant_name = response.json().get('name')

        self.assertEqual(restaurant_name, [
            'restaurant with this name already exists.'])

    def test_get_all_restaurant_success(self):
        restaurant = RestaurantFactory.create()

        response = self.client.get(
            reverse('categories:restaurants-list', ))

        category_name = response.json()[0].get('name')

        self.assertEqual(category_name, restaurant.name)

    def test_get_restaurant_by_id(self):
        restaurant = RestaurantFactory.create()
        response = self.client.get(
            reverse('categories:restaurants-detail',
                    args=(restaurant.pk,)))

        category_name = response.json().get('name')

        self.assertEqual(category_name, restaurant.name)

    def test_get_restaurant_by_invalid_id(self):
        response = self.client.get(
            reverse('categories:restaurants-detail',
                    args=(1,)))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json().get('detail'), 'Not found.')

    def test_update_restaurant(self):
        restaurant = RestaurantFactory.create()

        data = {
            'name': 'test_restaurant_updated',
            'address': restaurant.address
        }
        url = reverse('categories:restaurants-detail',
                      args=(restaurant.pk,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = response.json()
        restaurant_name = response.get('name')
        restaurant_address = response.get('address')
        self.assertEqual(restaurant_name, data.get('name'))
        self.assertEqual(restaurant_address, restaurant.address)

    def test_patch_restaurant(self):
        restaurant = RestaurantFactory.create()

        data = {
            'name': 'test_restaurant_updated'
        }
        url = reverse('categories:restaurants-detail',
                      args=(restaurant.pk,))
        response = self.client.patch(url, data=data,
                                     content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        response = response.json()
        restaurant_name = response.get('name')
        self.assertEqual(restaurant_name, data.get('name'))

    def test_update_restaurant_invalid_id(self):
        data = {
            'name': 'test_category_updated'
        }
        url = reverse('categories:restaurants-detail', args=(1,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.delete_topic')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.delete_queue')
    def test_delete_restaurant(self, mock_delete_queue, mock_delete_topic):
        category = RestaurantFactory.create()

        url = reverse('categories:restaurants-detail',
                      args=(category.pk,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.delete_topic')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.delete_queue')
    def test_delete_category_invalid_id(self, mock_delete_queue,
                                        mock_delete_topic):
        url = reverse('categories:restaurants-detail',
                      args=(1,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
