# Yemek Sepeti Python Projesi

### Proje Bilgileri

Proje Python 3.8 ile Django Framework'ü kullanılarak geliştirilmiştir. 

Veritabanı olarak PostgreSQL kullanılmıştır.

Pub/Sub işlemleri için AWS'te SNS ve SQS araçları kullanılmıştır. 

#### Proje Şeması

![yemeksepeti.drawio](doc/yemeksepeti.drawio.png)



### Yükleme ve Çalıştırma

Proje dockerize edilmiştir. Proje reposunda bulunan dockerfile veya docker-compose dosyası ile yüklenip çalıştırılabilir.
Proje postgresql veritabanına ihtiyaç duyduğu için docker-compose ile yüklemek daha kolay olacaktır. AWS'ye bağlanmak için gerekli olan bilgileri ve veritabanı bağlantı bilgilerini değiştirmek isterseniz ortam değişkenlerini kullanarak değiştirebilirsiniz. Aşağıdaki tabloda ortam değişkeni bilgilerini bulabilirsiniz.

| Ortam Değişkeni Anahtarı | Açıklama                                     | Varsayılan Değer                         |
| ------------------------ | -------------------------------------------- | ---------------------------------------- |
| AWS_ACCESS_KEY_ID        | AWS'ye erişmek için gerekli olan bilgi       | AKIAQI7MQW55C5WHDXRT                     |
| AWS_SECRET_ACCESS_KEY    | AWS'ye erişmek için gerekli olan bilgi       | HUiCcq7IMl39m1Tag7uUi0Nv5qKQXIee0foXsEqE |
| AWS_DEFAULT_REGION       | AWS'de araçların kullanılacağı bölge bilgisi | eu-central-1                             |
| DB_HOST                  | Veritabanı host bilgisi                      | yemeksepeti-postgres                     |
| DB_NAME                  | Veritabanı ismi                              | yemeksepeti                              |
| DB_USER                  | Veritabanı kullanıcı ismi                    | postgres                                 |
| DB_PASSWORD              | Veritabanı kullanıcısı şifresi               | 123456                                   |

### Komutlar

Komutlar docker-compose dosyanın olduğu dizinde çalıştırılmalıdır.

##### Projeyi çalıştırma

```
docker-compose up -d 
```

##### Projeyi durdurma

```
docker-compose down
```

### Veritabanı Şeması

Projede kullandığım database entityleri ve onların ilişkileri aşağıdaki gibidir.

![image-20211205143421543](doc/er_diagram.png)



### Api Dökümantasyonu

Projeyi çalıştırdıkdan sonra http://localhost:8000/swagger-ui/ adresinden swagger dökümantasyonuna erişebilirsiniz. Ayrıca proje deposunda bulunan Yemeksepeti_Case.postman_collection.json isimli Postman koleksiyonunu da Postman'a ekleyerek kullanabilirsiniz.