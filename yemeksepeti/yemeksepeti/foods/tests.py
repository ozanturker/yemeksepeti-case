from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from yemeksepeti.commons.factories.factory import (RestaurantFactory,
                                                   CategoryFactory,
                                                   FoodFactory)


class FoodViewTest(TestCase):

    def setUp(self) -> None:
        self.restaurant = RestaurantFactory.create()
        self.category = CategoryFactory.create()

    def test_create_food_success(self):
        data = {
            'name': 'test_category',
            'unit_price': '12.11',
            'category': self.category.pk,
            'restaurant': self.restaurant.pk
        }

        response = self.client.post(
            reverse('categories:foods-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        response = response.json()

        self.assertEqual(response.get('name'), data.get('name'))
        self.assertEqual(response.get('unit_price'), data.get('unit_price'))
        self.assertEqual(self.category.pk, data.get('category'))
        self.assertEqual(self.restaurant.pk, data.get('restaurant'))

    def test_create_food_invalid_data(self):
        data = {
            'name': 'test'
        }
        response = self.client.post(
            reverse('categories:foods-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        required_error = response.json().get('category')

        self.assertEqual(required_error, ['This field is required.'])

    def test_get_all_foods_success(self):
        food = FoodFactory.create(restaurant=self.restaurant,
                                  category=self.category)

        response = self.client.get(
            reverse('categories:foods-list', ))

        food_name = response.json()[0].get('name')

        self.assertEqual(food_name, food.name)

    def test_get_food_by_id(self):
        food = FoodFactory.create(restaurant=self.restaurant,
                                  category=self.category)
        response = self.client.get(
            reverse('categories:foods-detail',
                    args=(food.pk,)))

        food_name = response.json().get('name')

        self.assertEqual(food_name, food.name)

    def test_get_food_by_invalid_id(self):
        response = self.client.get(
            reverse('categories:foods-detail',
                    args=(1,)))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json().get('detail'), 'Not found.')

    def test_update_food(self):
        food = FoodFactory.create(category=self.category,
                                  restaurant=self.restaurant)

        data = {
            'name': 'test_food_updated',
            'category': self.category.pk,
            'restaurant': self.restaurant.pk,
            'unit_price': food.unit_price

        }
        url = reverse('categories:foods-detail',
                      args=(food.pk,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        food_name = response.json().get('name')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(food_name, data.get('name'))

    def test_update_food_invalid_id(self):
        data = {
            'name': 'test_food_updated'
        }
        url = reverse('categories:foods-detail', args=(1,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_food(self):
        food = FoodFactory.create(category=self.category,
                                  restaurant=self.restaurant)

        url = reverse('categories:foods-detail',
                      args=(food.pk,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_food_invalid_id(self):
        url = reverse('categories:foods-detail',
                      args=(1,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
