from rest_framework import viewsets

from yemeksepeti.foods.models import Food
from yemeksepeti.foods.serializers import FoodSerializer


class FoodViewSet(viewsets.ModelViewSet):
    queryset = Food.objects.all()
    serializer_class = FoodSerializer
