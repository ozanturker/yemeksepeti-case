from django.db import models

from yemeksepeti.foods.models import Food
from yemeksepeti.restaurants.models import Restaurant
from yemeksepeti.users.models import User


class OrderStatus(models.IntegerChoices):
    WAITING = 1
    COMPLETED = 2


class Order(models.Model):
    customer = models.ForeignKey(User, on_delete=models.RESTRICT)
    status = models.IntegerField(choices=OrderStatus.choices)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.RESTRICT)


class OrderItem(models.Model):
    order = models.ForeignKey(Order, on_delete=models.RESTRICT,
                              related_name='order_items')
    food = models.ForeignKey(Food, on_delete=models.RESTRICT)
    quantity = models.IntegerField()
