from abc import ABC

import boto3


class PubSubClient(ABC):

    def create_topic(self, data):
        raise NotImplementedError

    def subscribe_queue(self, topic_arn, queue_arn):
        raise NotImplementedError

    def publish_message(self, topic_arn, content):
        raise NotImplementedError


class SNSPubSubClient(PubSubClient):
    sns_client = boto3.client('sns')

    def create_topic(self, name):
        return self.sns_client.create_topic(Name=name)

    def subscribe_queue(self, topic_arn, queue_arn):
        self.sns_client.subscribe(TopicArn=topic_arn, Protocol='sqs',
                                  Endpoint=queue_arn)

    def publish_message(self, topic_arn, content):
        self.sns_client.publish(
            TopicArn=topic_arn,
            Message=content)

    def delete_topic(self, topic_arn):
        self.sns_client.delete_topic(
            TopicArn=topic_arn
        )
