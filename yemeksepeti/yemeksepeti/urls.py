"""yemeksepeti URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.routers import DefaultRouter
from rest_framework.schemas import get_schema_view

from yemeksepeti.categories.views import CategoryViewSet
from yemeksepeti.foods.views import FoodViewSet
from yemeksepeti.orders.views import OrderViewSet
from yemeksepeti.restaurants.views import RestaurantViewSet
from yemeksepeti.users.views import UserViewSet

_base_router = DefaultRouter()
_base_router.register(r'categories', CategoryViewSet, 'categories')
_base_router.register(r'restaurants', RestaurantViewSet, 'restaurants')
_base_router.register(r'foods', FoodViewSet, 'foods')
_base_router.register(r'orders', OrderViewSet, 'orders')
_base_router.register(r'users', UserViewSet, 'users')

urlpatterns = [
    path('admin/', admin.site.urls),
    url(r'api/v1/', include((_base_router.urls, 'categories'),
                            namespace='categories'), name='categories'),
    path('openapi', get_schema_view(
        title="Yemeksepeti case",
        version="1.0.0"
    ), name='openapi-schema'),
    path('swagger-ui/', TemplateView.as_view(
        template_name='swagger-ui.html',
        extra_context={'schema_url': 'openapi-schema'}
    ), name='swagger-ui'),
]
