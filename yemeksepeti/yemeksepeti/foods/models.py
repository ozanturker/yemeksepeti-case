from django.db import models

from yemeksepeti.categories.models import Category
from yemeksepeti.restaurants.models import Restaurant


class Food(models.Model):
    name = models.CharField(max_length=255)
    category = models.ForeignKey(Category, on_delete=models.RESTRICT)
    restaurant = models.ForeignKey(Restaurant, on_delete=models.RESTRICT)
    unit_price = models.DecimalField(max_digits=5, decimal_places=2)
