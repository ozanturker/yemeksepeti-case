from abc import ABC

import boto3


class QueueClient(ABC):

    def create_queue(self, data):
        raise NotImplementedError

    def get_messages(self, queue_name):
        raise NotImplementedError

    def delete_messages(self, queue_name, message):
        raise NotImplementedError


class SQSClient(QueueClient):
    sqs_client = boto3.client('sqs')

    def create_queue(self, name):
        return self.sqs_client.create_queue(QueueName=name)

    def get_messages(self, name):
        queue = self.sqs_client.get_queue_url(QueueName=name)
        return self.sqs_client.receive_message(QueueUrl=queue.get('QueueUrl'))

    def delete_messages(self, name, messages):
        if len(messages) > 0:
            queue = self.sqs_client.get_queue_url(QueueName=name)
            self.sqs_client.delete_message_batch(
                QueueUrl=queue.get('QueueUrl'), Entries=messages)

    def get_queue_arn(self, queue_url):
        result = self.sqs_client.get_queue_attributes(
            QueueUrl=queue_url, AttributeNames=['QueueArn'])
        return result.get('Attributes').get('QueueArn')

    def create_permission(self, queue_url, queue_arn, topic_arn):
        policy_document = """{{
          "Version":"2012-10-17",
          "Statement":[
            {{
              "Sid":"MyPolicy",
              "Effect":"Allow",
              "Principal" : {{"AWS" : "*"}},
              "Action":"SQS:SendMessage",
              "Resource": "{}",
              "Condition":{{
                "ArnEquals":{{
                  "aws:SourceArn": "{}"
                }}
              }}
            }}
          ]
        }}""".format(queue_arn, topic_arn)

        self.sqs_client.set_queue_attributes(
            QueueUrl=queue_url,
            Attributes={
                'Policy': policy_document
            }
        )

    def delete_queue(self, queue_name):
        response = self.sqs_client.get_queue_url(
            QueueName=queue_name,
        )
        self.sqs_client.delete_queue(
            QueueUrl=response.get('QueueUrl')
        )
