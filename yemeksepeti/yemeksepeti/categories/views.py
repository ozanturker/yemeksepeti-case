from rest_framework import viewsets

from yemeksepeti.categories.models import Category
from yemeksepeti.categories.serializers import CategorySerializer


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    http_method_names = ['get', 'post', 'delete', 'put']
