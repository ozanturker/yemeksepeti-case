import factory

from yemeksepeti.categories.models import Category
from yemeksepeti.foods.models import Food
from yemeksepeti.orders.models import Order, OrderStatus, OrderItem
from yemeksepeti.restaurants.models import Restaurant
from yemeksepeti.users.models import User


class CategoryFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Category

    name = 'category_1'


class RestaurantFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Restaurant

    name = 'test_restaurant'
    address = 'test_restaurant_address'


class FoodFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Food

    name = "test_food"
    unit_price = 12.34


class UserFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = User

    email = "testuser@example.com"


class OrderFactory(factory.django.DjangoModelFactory):
    class Meta:
        model = Order

    status = OrderStatus.WAITING


class OrderItemFactory(factory.Factory):
    class Meta:
        model = OrderItem
