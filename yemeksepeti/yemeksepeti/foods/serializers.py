from rest_framework import serializers

from yemeksepeti.foods.models import Food


class FoodSerializer(serializers.ModelSerializer):
    class Meta:
        model = Food
        fields = ['id', 'name', 'category', 'restaurant', 'unit_price']
