from rest_framework import serializers

from yemeksepeti.orders.models import Order, OrderItem, OrderStatus


class OrderItemCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ['food', 'quantity']


class OrderSerializer(serializers.ModelSerializer):
    order_items = OrderItemCreateSerializer(many=True, required=True)

    class Meta:
        model = Order
        fields = ['customer', 'status', 'restaurant', 'order_items']


class ProcessOrderSerializer(serializers.Serializer):
    restaurant_id = serializers.IntegerField()
    status = serializers.ChoiceField(choices=OrderStatus.choices)
