import json
from unittest.mock import patch

from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from yemeksepeti.commons.factories.factory import (UserFactory,
                                                   RestaurantFactory,
                                                   FoodFactory,
                                                   CategoryFactory,
                                                   OrderFactory)
from yemeksepeti.orders.models import OrderStatus, Order
from yemeksepeti.orders.services import OrderService


class OrderServiceTest(TestCase):

    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.publish_message')
    def test_create_order(self, mock_publish_message):
        mock_publish_message.return_value = None
        customer = UserFactory.create()
        restaurant = RestaurantFactory.create()
        category = CategoryFactory.create()
        food = FoodFactory.create(restaurant=restaurant, category=category)

        order_items = [
            {
                'food': food,
                'quantity': 1
            }
        ]

        service = OrderService()
        service.create_order(customer=customer, restaurant=restaurant,
                             status=OrderStatus.WAITING,
                             order_items=order_items)

        order = Order.objects.get(customer_id=customer.pk)
        self.assertEqual(order.restaurant.pk, restaurant.pk)
        self.assertEqual(order.status, OrderStatus.WAITING)
        self.assertEqual(order.order_items.all()[0].food.name, food.name)

    @patch('yemeksepeti.commons.clients.queue_client.SQSClient'
           '.delete_messages')
    @patch('yemeksepeti.commons.clients.queue_client.SQSClient.get_messages')
    def test_process_order(self, mock_get_messages, mock_delete_messages):
        customer = UserFactory.create()
        restaurant = RestaurantFactory.create()
        OrderFactory.create(pk=1, customer=customer, restaurant=restaurant)

        mock_get_messages.return_value = self._read_mock_file()

        service = OrderService()
        service.process_order(restaurant_id=restaurant.pk,
                              status=OrderStatus.COMPLETED)

    def _read_mock_file(self):
        with open("yemeksepeti/commons/mocks/sqs_mock.json", "r") as file:
            return json.loads(file.read())


class OrderViewTest(TestCase):

    def setUp(self) -> None:
        self.customer = UserFactory.create()
        self.restaurant = RestaurantFactory.create()
        self.category = CategoryFactory.create()
        self.food = FoodFactory.create(category=self.category,
                                       restaurant=self.restaurant)

    @patch('yemeksepeti.commons.clients.pubsub_client.SNSPubSubClient'
           '.publish_message')
    def test_create_order_success(self, mock):
        data = {
            "customer": self.customer.pk,
            "restaurant": self.restaurant.pk,
            "status": 1,
            "order_items": [
                {
                    "food": self.food.pk,
                    "quantity": 1
                }
            ]
        }
        response = self.client.post(
            reverse('categories:orders-list'), data=data,
            content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        response = response.json()
        customer = response.get('customer')
        status_code = response.get('status')
        self.assertEqual(customer, self.customer.pk)
        self.assertEqual(status_code, OrderStatus.WAITING)

    def test_create_category_invalid_data(self):
        data = {
            "restaurant": self.restaurant.pk,
            "status": 1,
            "order_items": [
                {
                    "food": self.food.pk,
                    "quantity": 1
                }
            ]
        }
        response = self.client.post(
            reverse('categories:orders-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        required_error = response.json().get('customer')

        self.assertEqual(required_error, ['This field is required.'])

    def test_get_all_orders_success(self):
        order_1 = OrderFactory.create(customer=self.customer,
                                      restaurant=self.restaurant)
        order_2 = OrderFactory.create(customer=self.customer,
                                      restaurant=self.restaurant,
                                      status=OrderStatus.COMPLETED)

        response = self.client.get(
            reverse('categories:orders-list', ))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        order_1_result = response.json()[0]
        order_2_result = response.json()[1]

        self.assertEqual(order_1.customer.pk, order_1_result['customer'])
        self.assertEqual(order_1.restaurant.pk, order_1_result['restaurant'])
        self.assertEqual(order_1.status, order_1_result['status'])
        self.assertEqual(order_2.status, order_2_result['status'])

    def test_get_all_waiting_orders_success(self):
        order_1 = OrderFactory.create(customer=self.customer,
                                      restaurant=self.restaurant)
        OrderFactory.create(customer=self.customer,
                            restaurant=self.restaurant,
                            status=OrderStatus.COMPLETED)

        response = self.client.get(
            reverse('categories:orders-list', ), {'status': 1})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        order_1_result = response.json()[0]

        self.assertEqual(len(response.json()), 1)
        self.assertEqual(order_1.customer.pk, order_1_result['customer'])
        self.assertEqual(order_1.restaurant.pk, order_1_result['restaurant'])
        self.assertEqual(order_1.status, order_1_result['status'])

    def test_get_all_finished_orders_success(self):
        OrderFactory.create(customer=self.customer,
                            restaurant=self.restaurant)
        order_1 = OrderFactory.create(customer=self.customer,
                                      restaurant=self.restaurant,
                                      status=OrderStatus.COMPLETED)

        response = self.client.get(
            reverse('categories:orders-list', ), {'status': 2})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        order_1_result = response.json()[0]

        self.assertEqual(len(response.json()), 1)
        self.assertEqual(order_1.customer.pk, order_1_result['customer'])
        self.assertEqual(order_1.restaurant.pk, order_1_result['restaurant'])
        self.assertEqual(order_1.status, order_1_result['status'])

    def test_get_orders_by_id(self):
        order = OrderFactory.create(customer=self.customer,
                                    restaurant=self.restaurant)
        response = self.client.get(
            reverse('categories:orders-detail',
                    args=(order.pk,)))

        customer = response.json().get('customer')

        self.assertEqual(customer, order.customer.pk)

    def test_get_orders_by_invalid_id(self):
        response = self.client.get(
            reverse('categories:orders-detail',
                    args=(1,)))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json().get('detail'), 'Not found.')
