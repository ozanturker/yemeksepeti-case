from django.test import TestCase
from django.urls import reverse
from rest_framework import status

from yemeksepeti.commons.factories.factory import CategoryFactory


class CategoryViewTest(TestCase):

    def test_create_category_success(self):
        data = {
            'name': 'test_category'
        }
        response = self.client.post(
            reverse('categories:categories-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        category_name = response.json().get('name')

        self.assertEqual(category_name, data.get('name'))

    def test_create_category_invalid_data(self):
        data = {
            'category': 'test_category'
        }
        response = self.client.post(
            reverse('categories:categories-list'), data=data)

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        required_error = response.json().get('name')

        self.assertEqual(required_error, ['This field is required.'])

    def test_create_category_duplicated_data(self):
        category = CategoryFactory.create()
        data = {
            'name': category.name
        }
        response = self.client.post(
            reverse('categories:categories-list'), data=data)

        category_name = response.json().get('name')

        self.assertEqual(category_name, [
            'category with this name already exists.'])

    def test_get_all_categories_success(self):
        category = CategoryFactory.create()

        response = self.client.get(
            reverse('categories:categories-list', ))

        category_name = response.json()[0].get('name')

        self.assertEqual(category_name, category.name)

    def test_get_category_by_id(self):
        category = CategoryFactory.create()
        response = self.client.get(
            reverse('categories:categories-detail',
                    args=(category.pk,)))

        category_name = response.json().get('name')

        self.assertEqual(category_name, category.name)

    def test_get_category_by_invalid_id(self):
        response = self.client.get(
            reverse('categories:categories-detail',
                    args=(1,)))

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(response.json().get('detail'), 'Not found.')

    def test_update_category(self):
        category = CategoryFactory.create()

        data = {
            'name': 'test_category_updated'
        }
        url = reverse('categories:categories-detail',
                      args=(category.pk,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        category_name = response.json().get('name')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(category_name, data.get('name'))

    def test_update_category_invalid_id(self):
        data = {
            'name': 'test_category_updated'
        }
        url = reverse('categories:categories-detail', args=(1,))
        response = self.client.put(url, data=data,
                                   content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_delete_category(self):
        category = CategoryFactory.create()

        url = reverse('categories:categories-detail',
                      args=(category.pk,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_delete_category_invalid_id(self):
        url = reverse('categories:categories-detail',
                      args=(1,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
