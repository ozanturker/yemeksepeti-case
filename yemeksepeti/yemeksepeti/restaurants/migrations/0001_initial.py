# Generated by Django 3.2.9 on 2021-12-05 10:09

from django.db import migrations, models


class Migration(migrations.Migration):
    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Restaurant',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True,
                                           serialize=False,
                                           verbose_name='ID')),
                ('code', models.CharField(max_length=255, unique=True)),
                ('name', models.CharField(max_length=255, unique=True)),
                ('address', models.TextField()),
                ('order_topic_address',
                 models.CharField(max_length=255, unique=True)),
                ('order_queue_address',
                 models.CharField(max_length=255, unique=True)),
            ],
        ),
    ]
